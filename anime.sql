/*
SQLyog Community
MySQL - 10.4.11-MariaDB : Database - anime
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`anime` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `anime`;

/*Table structure for table `judul` */

DROP TABLE IF EXISTS `judul`;

CREATE TABLE `judul` (
  `id_judul` int(11) NOT NULL AUTO_INCREMENT,
  `judul_anime` varchar(30) NOT NULL,
  PRIMARY KEY (`id_judul`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `judul` */

insert  into `judul`(`id_judul`,`judul_anime`) values 
(1,'Bleach'),
(2,'Naruto Shippuden'),
(3,'One Piece');

/*Table structure for table `karakter` */

DROP TABLE IF EXISTS `karakter`;

CREATE TABLE `karakter` (
  `id_karakter` int(11) NOT NULL,
  `nama_karakter` varchar(30) NOT NULL,
  `id_judul` int(11) NOT NULL,
  `photos` varchar(30) DEFAULT NULL,
  `seiyuu` varchar(30) DEFAULT NULL,
  `studio` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_karakter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `karakter` */

insert  into `karakter`(`id_karakter`,`nama_karakter`,`id_judul`,`photos`,`seiyuu`,`studio`) values 
(1,'Naruto Uzumaki',2,'narutouzumaki.jpg','Junko Takeuchi','Studio Pierrot');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
