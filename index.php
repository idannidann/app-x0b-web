<?php
    $DB_NAME = "anime";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT k.id_karakter, k.nama_karakter, j.judul_anime, k.seiyuu, k.studio, k.photos
            FROM karakter k, judul j 
            WHERE k.id_judul = j.id_judul";
    
    $result = mysqli_query($conn,$sql);
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Karakter Anime</title>
</head>
<br>
<body>
    <div class="container">
        <h1>Data Karakter Anime</h1>
        <div class="table-responsive">
            <table class="table table-bordered table-striped bg-light">
                <thead class="table-striped table-dark" style="text-align: center;">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">ID Karakter</th>
                        <th scope="col">Nama Karakter</th>
                        <th scope="col">Judul Anime</th>
                        <th scope="col">Nama Seiyuu</th>
                        <th scope="col">Nama Studio</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <?php
                    $no=1;
                    while($pst = mysqli_fetch_assoc($result)){
                ?>
                <tbody>
                    <tr>
                        <th scope="row" style="text-align: center;"><?php echo $no;$no++; ?>.</th>
                        <td><?php echo $pst['id_karakter']; ?></td>
                        <td><?php echo $pst['nama_karakter']; ?></td>
                        <td><?php echo $pst['judul_anime']; ?></td>
                        <td><?php echo $pst['seiyuu']; ?></td>
                        <td><?php echo $pst['studio']; ?></td>
                        <td style="text-align: center;">
                            <img src="images/<?php echo $pst['photos']; ?>" style="width: 200px;" alt="Foto Karakter">
                        </td>
                        <td style="text-align: center;"><?php echo "<a class='btn btn-danger' href='proses_hapus.php?id_karakter=".$pst['id_karakter']."'>Hapus</a>"; ?></td>
                    </tr>
                    </tr>
                </tbody>
                <?php } ?>
            </table>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>