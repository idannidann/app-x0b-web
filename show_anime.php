<?php 

$DB_NAME = "anime";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $judul_anime = $_POST['judul_anime'];
    if(empty($judul_anime)){
        $sql = "select * from judul order by judul_anime asc";
    }else{
        $sql = "select * from judul where judul_anime like '%$judul_anime%' order by judul_anime asc";
    }
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $judul_anime = array();
        while ($jd_anime = mysqli_fetch_assoc($result)) {
            array_push($judul_anime, $jd_anime);
        }
        echo json_encode($judul_anime);
    }
}