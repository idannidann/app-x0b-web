<?php

$DB_NAME = "anime";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $sql = "SELECT k.id_karakter, k.nama_karakter, j.judul_anime, k.seiyuu, k.studio, 
            concat('http://192.168.43.53/karakter/images/',k.photos) as url
            from karakter k, judul j
            where k.id_judul = j.id_judul";

    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $data_posting = array();
        while ($posting = mysqli_fetch_assoc($result)) {
            array_push($data_posting, $posting);
        }
        echo json_encode($data_posting);
    }
}
