<?php

$DB_NAME = "anime";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $mode = $_POST['mode'];
    $respon = array(); $respon['kode'] = '000';
    switch($mode){
        case "insert":
            $id_karakter = $_POST['id_karakter'];
            $nama_karakter = $_POST['nama_karakter'];
            $judul_anime = $_POST['judul_anime'];
            $seiyuu = $_POST['seiyuu'];
            $studio = $_POST['studio'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/";

            $sql = "select id_judul from judul where judul_anime = '$judul_anime'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_judul = $data['id_judul'];

                $sql = "insert into karakter(id_karakter, nama_karakter, id_judul, seiyuu, studio, photos)
                values('$id_karakter','$nama_karakter','$id_judul','$seiyuu','$studio','$file')";

                $result = mysqli_query($conn,$sql);
                if($result){
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $sql = "delete from karakter where id_karakter = '$id_karakter'";
                        mysqli_query($conn,$sql);
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        echo json_encode($respon); exit();
                    }
                }else{
                    $respon['kode'] = "111";
                    echo json_encode($respon); exit();
                }
            }
        break;
        case "update":
            $id_karakter = $_POST['id_karakter'];
            $nama_karakter = $_POST['nama_karakter'];
            $judul_anime = $_POST['judul_anime'];
            $seiyuu = $_POST['seiyuu'];
            $studio = $_POST['studio'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/";

            $sql = "select id_judul from judul where judul_anime = '$judul_anime'";
            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_judul = $data['id_judul'];

                $sql = "";
                if($imstr == ""){
                    $sql = "update karakter set nama_karakter='$nama_karakter',
                            id_judul='$id_judul',
                            seiyuu='$seiyuu',
                            studio='$studio'
                            where id_karakter='$id_karakter'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit();
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }else{
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        $sql = "update karakter set nama_karakter='$nama_karakter',
                        id_judul='$id_judul',
                        seiyuu='$seiyuu',
                        studio='$studio',
                        photos = '$file'
                        where id_karakter='$id_karakter'";
                       
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }
                }
            }
        break;
        case "delete":
            $id_karakter =$_POST['id_karakter'];
            $sql ="select photos from karakter where id_karakter='$id_karakter'";
            $result = mysqli_query($conn,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photos = $data['photos'];
                    $path = "images/";
                    unlink($path.$photos);
                }
                $sql = "delete from karakter where id_karakter = '$id_karakter'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    echo json_encode($respon); exit();
                }else{
                    $respon['kode'] = '111';
                    echo json_encode($respon); exit();
                }
            }
        break;
    }
}